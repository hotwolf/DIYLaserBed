---
layout: post
title:  Project Started
---

This is a laserbed design for K40 laser cutter clones. It is supposed to be easy to rebuild,
as it only uses common mechanical components and a few simple 3D-printed parts.  
