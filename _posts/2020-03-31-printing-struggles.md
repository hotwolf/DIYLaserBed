---
layout: post
title:  Printing Struggles
---

While waiting for some missing vitamins to arrive by mail, I've been working
on the 3D prints. For many 3D printers these parts are probably no issue, but
my Prusa clone has struggles with my design.
For example, the lower bearing hoder has a couple couple of layers low print
volume. At these layers, the filament does not adhere well. 

![misprints]({{ site.url }}/images/printing_struggles.jpg)

I think I need to make this design more printer fiendly.
