---
layout: post
title:  New Printer
---

As it turns out, I didn't need to make my design more printer friendly.
I just needed to buy a decent printer.

![3D prints]({{ site.url }}/images/3D_prints.jpg)

My printing struggles are over. My new printer produces the DIYLaserBed
parts just fine.
